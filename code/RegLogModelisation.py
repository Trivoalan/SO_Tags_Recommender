import pickle
import pandas as pd
import numpy as np

#!pip install SnowballStemmer
#!pip install nltk
#!pip install beautifulsoup4
#!pip install spacy
#!python -m spacy download en
import nltk
#nltk.download('stopwords')
#nltk.download('punkt')

import re
from nltk.tokenize import MWETokenizer
mwtokenizer = nltk.MWETokenizer(separator='')
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from bs4 import BeautifulSoup
import spacy
lemmatizer = spacy.load('en', disable=['parser', 'ner'])
REPLACE_BY_SPACE_RE = re.compile(r'[:/(){}\[\]\|@,!;\?"\']')
BAD_SYMBOLS_RE = re.compile(r'[^0-9a-z +#_\-\.]')
STOPWORDS = set(stopwords.words('english'))
stemmer = PorterStemmer()
excludedWords = ['use', 'file', 'get', 'like', 'would', 'function', 'class', 'difference', 'value',
                'name','object', 'set', 'error', 'public', 'create', 'active', 'variable', 'understand', 
                'work', 'problem', 'issue', 'element', 'index', 'var', 'directory', 'folder', 'source',
                'not', 'can', 'int', 'program', 'value', 'default', 'one', 'two', 'versus', 'many', 'way',
                'say', 'want', 'whose', 'know', 'string', 'return', 'method', 'string', 'code', 'example',
                'fail', 'quite', 'app', 'user', 'application', 'request', 'try', 'find', 'new']

def clean_text(text):
    text = re.sub(r'<code>.*<\/code>', ' ', text)
    text = re.sub(r'<a.*<\/a>', ' ', text)
    text = re.sub(r"\bhttps:\//[a-z0-9.]*", ' ', text)
    text = re.sub(r"\bhttp:\//[a-z0-9.]*", ' ', text)
    soup = BeautifulSoup(text,"lxml") # HTML decoding
    text = soup.get_text(separator=' ')
    text = text.lower() # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ', text) # replace REPLACE_BY_SPACE_RE symbols by space in text
    text = BAD_SYMBOLS_RE.sub('', text) # delete symbols which are in BAD_SYMBOLS_RE from text
    text = ' '.join([word.lemma_ for word in lemmatizer(text) if word.lemma_ != '-PRON-'])
    text = ' '.join(word for word in text.split() if ((word not in STOPWORDS) and (word not in excludedWords))) # delete stopwords and excluded words from text
    text = text.replace(' - ', ' ')
    text = text.replace(' .', ' ')
    text = text.replace(' ', ' ')
    text = re.sub(r'(?<=[a-z])\.(?=[a-z])', ' ', text)
    text = re.sub(r'c #', ' c# ', text)
    text = re.sub(r' net ', ' .net ', text)
    text = re.sub(r'objective - c', 'objective-c', text)
    #text = ' '.join([stemmer.stem(word) for word in text.split()])
    text = ' '.join(word for word in text.split() if not word.isdigit())
    text = ' '.join(word for word in text.split() if (len(word) >= 2 or word == "c" or word == "r"))
    mwtokenizer = nltk.MWETokenizer(separator='')
    mwtokenizer.add_mwe(('c', '#'))
    text = mwtokenizer.tokenize(text.split())
    return text

import warnings
warnings.filterwarnings('always')

def result_printer(y_train, ytr_pred, y_test, y_pred):
    print('Train Set :')
    print("Accuracy :",accuracy_score(y_train,ytr_pred))
    F1_train = f1_score(y_train,ytr_pred, average = 'micro')
    print("Micro f1 Score :", F1_train)
    print("Hamming Loss :",hamming_loss(y_train,ytr_pred))
    print("Jaccard Score :",jaccard_score(y_train, ytr_pred, average='macro'))
    print('============')
    print('Test Set :')
    print("Accuracy :",accuracy_score(y_test,y_pred))
    F1_test = f1_score(y_test,y_pred, average = 'micro')
    print("Micro f1 Score :", F1_test)
    print("Hamming Loss :",hamming_loss(y_test,y_pred))
    print("Jaccard Score :",jaccard_score(y_test, y_pred, average='macro'))
    return [F1_train, F1_test]

from sklearn.model_selection import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.metrics import f1_score, accuracy_score, hamming_loss, jaccard_score
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split
from collections import Counter

#Résultats de la requête StackOverflow : 40000 questions et réponses de qualité
results = pd.read_csv("./QueryResults.csv", ",")
df = results.copy()

#Pré-processing des tags (simple)
df["tags"] = df["tags"].str.replace('><', ' ')
df["tags"] = df["tags"].str.replace('<', '')
df["tags"] = df["tags"].str.replace('>', '')

#Texte = corps + titre des posts avec poids du titre multiplié par 3
df["text"] = df["title"]+" "+df["title"]+" "+df["title"]+" "+df["body"]
df_data = df[["tags","text"]].copy()

#Préprocessing du texte (Lemmatize, html tags, STOPWORDS, caractères spéciaux, Tokenisation...)
clean_df = df_data.copy()
clean_df['processedText'] = clean_df['text'].apply(clean_text)

#Filtrage des tags, on ne garde que les 200 tags les plus utilisés (couvre 95% des questions)
selectedTags = Counter(" ".join(clean_df['tags']).split()).most_common(200)
selectedTags = pd.DataFrame(selectedTags)
selectedTags.rename(columns={0:'tags'}, inplace=True)
selectedTags['tags'].to_numpy()
filtered_df = clean_df.copy()
filtered_df['tags'] = [' '.join(w for w in tags.split() if w in selectedTags['tags'].to_numpy()) for tags in filtered_df['tags']]

#Tokenisation des tags en conservant le tag C#
mwtokenizer = nltk.MWETokenizer(separator='')
mwtokenizer.add_mwe(('c', '#'))
filtered_df['tags'] = filtered_df.apply(lambda row: mwtokenizer.tokenize(row['tags'].split()), axis=1)

#One Hot Encoder
mlb = MultiLabelBinarizer(sparse_output=True)

#Train/Test Split + One Hot Encoder
X_train, X_test, y_train, y_test = train_test_split(filtered_df['processedText'], mlb.fit_transform(filtered_df['tags']), test_size = 0.2,random_state = 40)
#print("Number of data points in training data :", X_train.shape[0])
#print("Number of data points in test data :", X_test.shape[0])

# save the MultiLabelBinarizer
with open('SO_MLB.pkl', 'wb') as fid:
    pickle.dump(mlb, fid)
    
    X_train = X_train.apply(lambda x: " ".join(x))
X_test = X_test.apply(lambda x: " ".join(x))

#Count Vectoriser + tfidf transformer combiné (avec paramètres déjà optimisés par gridsearch)
from sklearn.feature_extraction.text import TfidfVectorizer

vect = TfidfVectorizer( norm='l2',
                        min_df=3,
                        use_idf=True,
                        smooth_idf=True,
                        ngram_range=(1,2), 
                        preprocessor=None,
                        stop_words=None,
                        lowercase=False,
                        token_pattern=r'(?u)\b\w+\b',
                        tokenizer=None,
                        max_features=None,
                        sublinear_tf=True )

X_train_multilabel = vect.fit_transform(X_train)

import pickle
# save the classifier
with open('SO_TfidfVectorizer.pkl', 'wb') as fid:
    pickle.dump(vect, fid)
    
X_test_multilabel = vect.transform(X_test)

#Configuration optimale de l'algorithme champion (paramètres déjà optimisés par gridsearch)
model_to_tune = OneVsRestClassifier(
                        LogisticRegression(
                             random_state=42,
                             max_iter=500,
                             C=5,
                             class_weight='balanced',
                             fit_intercept=True, 
                             solver='liblinear',
                             penalty='l2' ))

#Cross validation 10 fold (sans grille de paramètre, pas nécessaire)
clf = GridSearchCV(model_to_tune, param_grid={},  cv=8, scoring='f1_micro', n_jobs=-1, verbose=100, return_train_score = True)
clf.fit(X_train_multilabel, y_train)
print (clf.best_params_)

#Prédictions sur le train set et le test set
#ytr_pred = clf.predict(X_train_multilabel)
#y_pred = clf.predict(X_test_multilabel)

#result_printer(y_train, ytr_pred, y_test, y_pred)

# save the classifier
with open('SO_model_regLog_opt.pkl', 'wb') as fid:
    pickle.dump(clf, fid)    