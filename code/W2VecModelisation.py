import pickle
import pandas as pd
import numpy as np
from gensim.models import Word2Vec

#Résultats de la requête StackOverflow : 40000 questions et réponses de qualité
results = pd.read_csv("./QueryResults.csv", ",")
df = results.copy()

df["tags"] = df["tags"].str.replace('><', ' ')
df["tags"] = df["tags"].str.replace('<', '')
df["tags"] = df["tags"].str.replace('>', '')
df_word2vec = df["tags"].copy()
df_word2vec = df_word2vec.str.split(" ")
df_word2vec.values

modelW2V = Word2Vec(df_word2vec.values, min_count=3)

modelW2V.save('TagsWord2Vec.pkl')