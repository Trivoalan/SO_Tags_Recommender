import streamlit as st 

import re
import nltk
import spacy
import bs4
import numpy as np

from nltk.tokenize import MWETokenizer
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from bs4 import BeautifulSoup

# ML Pkg
# from sklearn.externals import joblib
import pickle

# load supervised_mlb
with open("models/SO_MLB.pkl", "rb") as f:
    supervised_mlb = pickle.load(f)
    
# load supervised_vectorizer
with open("models/SO_TfidfVectorizer.pkl", "rb") as f:
    supervised_vectorizer = pickle.load(f)

# load Logistic Regression Model
with open("models/SO_model_regLog_opt.pkl", "rb") as f:
    supervised_clf = pickle.load(f)

# load Word2Vec Model
with open("models/SO_TagsWord2Vec.pkl", "rb") as f:
    w2v = pickle.load(f)


lemmatizer = spacy.load('en', disable=['parser', 'ner'])
REPLACE_BY_SPACE_RE = re.compile(r'[:/(){}\[\]\|@,!;\?"\']')
BAD_SYMBOLS_RE = re.compile(r'[^0-9a-z +#_\-\.]')
STOPWORDS = set(stopwords.words('english'))
stemmer = PorterStemmer()
excludedWords = ['use', 'file', 'get', 'like', 'would', 'function', 'class', 'difference', 'value',
                'name','object', 'set', 'error', 'public', 'create', 'active', 'variable', 'understand', 
                'work', 'problem', 'issue', 'element', 'index', 'var', 'directory', 'folder', 'source',
                'not', 'can', 'int', 'program', 'value', 'default', 'one', 'two', 'versus', 'many', 'way',
                'say', 'want', 'whose', 'know', 'string', 'return', 'method', 'string', 'code', 'example',
                'fail', 'quite', 'app', 'user', 'application', 'request', 'try', 'find', 'new']

def clean_text(text):
    text = re.sub(r'<code>.*<\/code>', ' ', text)
    text = re.sub(r'<a.*<\/a>', ' ', text)
    text = re.sub(r"\bhttps:\//[a-z0-9.]*", ' ', text)
    text = re.sub(r"\bhttp:\//[a-z0-9.]*", ' ', text)
    soup = BeautifulSoup(text,"lxml") # HTML decoding
    text = soup.get_text(separator=' ')
    text = text.lower() # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ', text) # replace REPLACE_BY_SPACE_RE symbols by space in text
    text = BAD_SYMBOLS_RE.sub('', text) # delete symbols which are in BAD_SYMBOLS_RE from text
    #text = ' '.join([word.lemma_ for word in lemmatizer(text)])
    text = ' '.join([word.lemma_ for word in lemmatizer(text) if word.lemma_ != '-PRON-'])
    text = ' '.join(word for word in text.split() if ((word not in STOPWORDS) and (word not in excludedWords))) # delete stopwords and excluded words from text
    text = text.replace(' - ', ' ')
    text = text.replace(' .', ' ')
    text = text.replace(' ', ' ')
    text = re.sub(r'(?<=[a-z])\.(?=[a-z])', ' ', text)
    text = re.sub(r'c #', ' c# ', text)
    text = re.sub(r' net ', ' .net ', text)
    text = re.sub(r'objective - c', 'objective-c', text)
    #text = ' '.join([stemmer.stem(word) for word in text.split()])
    text = ' '.join(word for word in text.split() if not word.isdigit())
    text = ' '.join(word for word in text.split() if (len(word) >= 2 or word == "c" or word == "r"))
    mwtokenizer = nltk.MWETokenizer(separator='')
    mwtokenizer.add_mwe(('c', '#'))
    text = mwtokenizer.tokenize(text.split())
    return text


# Prediction
def predictTagsFromText_supervised(text):
    cleaned_text = ' '.join(word for word in clean_text(str(text)))
    TfIdfText = supervised_vectorizer.transform([cleaned_text])
    encoded_y_pred = supervised_clf.predict(TfIdfText)
    tags = supervised_mlb.inverse_transform(encoded_y_pred)
    return tags

def arrayToString(array):
    i = 0
    for x in array:
        if (i == 0): outputStr = "" + x[0]
        else : outputStr += ", "+  x[0]
        i += 1
    return outputStr

def predictTagsFromOtherTags_w2v(tags):
    word2vec_result = w2v.most_similar(positive=tags, topn=5)
    i = 0
    for x in word2vec_result:
        if (i == 0): w2vStr = "" + x[0]
        else : w2vStr += ", "+  x[0]
        i += 1
    return w2vStr


def main():
    st.title("Stack Overflow Tags Recommender")
    title = st.text_input("Enter Title","")
    body = st.text_area("Enter Body","")
    if st.button("Try"):
        text = title+ " " +body
        reglog_result = predictTagsFromText_supervised([text])
        w2vString = predictTagsFromOtherTags_w2v(np.asarray(list(reglog_result[0])))
        reglogString = arrayToString (reglog_result) 

        st.text("Recommended Tag(s): ")
        st.text(reglogString)
        st.text("Other suggestions: ")
        st.text(w2vString)

if __name__ == '__main__':
	main()